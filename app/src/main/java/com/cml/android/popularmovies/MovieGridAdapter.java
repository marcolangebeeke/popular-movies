package com.cml.android.popularmovies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.cml.android.popularmovies.Api.ApiMovie;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Adapter for the movie grid to be used in the RecyclerView
 */
public class MovieGridAdapter extends RecyclerView.Adapter<MovieGridAdapter.ViewHolder> {

    /**
     * Log tag
     */
    private static final String LOG_TAG = MovieGridAdapter.class.getSimpleName();

    /**
     * Context the adapter is created in
     */
    private Context mContext;

    /**
     * a list of movie objects
     */
    private List<ApiMovie> mMovies;

    /**
     *
     */
    private final MovieGridAdapterOnClickHandler mItemClickHandler;

    /**
     * The interface that receives onClick messages.
     */
    public interface MovieGridAdapterOnClickHandler {
        void onClick(ApiMovie movie);
    }

    /**
     * Constructor
     */
    MovieGridAdapter(Context context, MovieGridAdapterOnClickHandler handler) {
        mContext = context;
        mItemClickHandler = handler;
    }

    /**
     * Inflate a movie grid item viewholder
     *
     * @param parent   The RecyclerView for the movie grid
     * @param viewType Type of view
     * @return The inflated viewholder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // get the inflater from the recyclerview context and inflate a grid item layout
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.movie_grid_item, parent, false);

        return new ViewHolder(view);
    }

    /**
     * Populate the ViewHolder items with data from the movie List
     *
     * @param holder   The ViewHolder containing the grid item view
     * @param position The position of the item in the RecyclerView
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (getItemCount() >= position) {

            // load and set the movie poster image using Picasso
            String posterPath = mMovies.get(position).getPoster_path();
            String imageUrl = mContext.getString(R.string.tmdb_image_base_url)
                    + mContext.getString(R.string.tmdb_image_format_medium)
                    + posterPath;
            Picasso.with(mContext)
                    .load(imageUrl)
                    .error(R.drawable.tmdb_icon_small)
                    .into(holder.mMoviePoster);

            // set the image contentdescription to the title of the movie
            String movieTitle = mMovies.get(position).getTitle();
            holder.mMoviePoster.setContentDescription(movieTitle);
        }
    }

    /**
     * Get the amount of items in the movie List
     *
     * @return The amount of items
     */
    @Override
    public int getItemCount() {
        if (mMovies != null) {
            return mMovies.size();
        } else {
            return 0;
        }
    }

    /**
     * Set the movie List and notify the registered observers that the data has changed
     *
     * @param movies List of ApiMovie objects
     */
    void setMovies(List<ApiMovie> movies) {
        mMovies = movies;
        notifyDataSetChanged();
    }

    /**
     * Empty the movie List and notify the registered observers that the data has changed
     */
    void clearMovies() {
        if (mMovies != null) {
            mMovies.clear();
            notifyDataSetChanged();
        }
    }

    /**
     * Movie grid item ViewHolder used in the RecyclerView
     */
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /**
         * ImageView for the movie poster
         */
        ImageView mMoviePoster;

        /**
         * Constructor to find the view items
         *
         * @param view The View item
         */
        ViewHolder(View view) {
            super(view);
            mMoviePoster = view.findViewById(R.id.movie_poster);
            view.setOnClickListener(this);
        }

        /**
         * Catch the OnClick event on the view and pass it on to the ClickHandler with the ApiMovie
         * object for clicked position
         *
         * @param view The clicked View
         */
        @Override
        public void onClick(View view) {
            int adapterPosition = getAdapterPosition();
            mItemClickHandler.onClick(mMovies.get(adapterPosition));
        }
    }
}
