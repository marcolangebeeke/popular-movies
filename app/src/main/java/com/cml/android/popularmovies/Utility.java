package com.cml.android.popularmovies;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Class containing utility functions
 */
class Utility {

    /**
     * Check if the network is available
     *
     * @return boolean
     */
    static boolean isNetworkAvailable(Context context) {

        // get the connectivity manager service
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // return true if we have networkinfo and are connected
        return (cm != null && cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected());
    }
}
