package com.cml.android.popularmovies.Api;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * TMDb Api interface for definition of which Api calls are available and how they should be called
 */
public interface TMDbApi {

    /**
     * Get the popular movies from the api
     *
     * @param apiKey The mandatory Api key
     * @return A JsonObject containing a JsonElement that is an array of movie objects sorted
     * by popularity
     */
    @GET("movie/popular")
    Call<JsonObject> getPopularMovies(@Query("api_key") String apiKey);

    /**
     * Get the top rated movies from the api
     *
     * @param apiKey The mandatory Api key
     * @return A JsonObject containing a JsonElement that is an array of movie objects sorted
     * by highest rating
     */
    @GET("movie/top_rated")
    Call<JsonObject> getTopRatedMovies(@Query("api_key") String apiKey);
}
