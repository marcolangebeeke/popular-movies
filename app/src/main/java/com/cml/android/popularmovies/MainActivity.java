package com.cml.android.popularmovies;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cml.android.popularmovies.Api.ApiMovie;
import com.cml.android.popularmovies.Api.TMDbApi;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * MainActivity for displaying the movie posters loaded from the TMDb Api in a grid layout
 */
public class MainActivity extends AppCompatActivity implements MovieGridAdapter.MovieGridAdapterOnClickHandler {

    /**
     * Log tag
     */
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    /**
     * Movie list sort order options
     */
    private static final String MOVIE_SORT_POPULAR = "popular";
    private static final String MOVIE_SORT_TOP_RATED = "top_rated";

    /**
     * Name for the intent extra ApiMovie Parcelable
     */
    public static final String INTENT_EXTRA_MOVIE = "extra_movie";

    /**
     * SavedInstance keys
     */
    private static final String INSTANCE_STATE_KEY_SORT_ORDER = "movie_sort_order";

    /**
     * The RecyclerView showing the movie posters
     */
    private RecyclerView mMovieGrid;

    /**
     * The Adapter for the movie grid RecyclerView
     */
    private MovieGridAdapter mMovieGridAdapter;

    /**
     * ProgressBar for feedback about loading of the movie list from the api
     */
    private ProgressBar mProgressBar;

    /**
     * Error TextView to inform the user if something has gone wrong
     */
    private TextView mErrorTextView;

    /**
     * Selected/default movie sort order
     */
    private String mMovieSortOrder = MOVIE_SORT_POPULAR;

    /**
     * Instantiate and setup the various components, and call getMovies to load the movie list
     * from the TMDb Api
     *
     * @param savedInstanceState The state of the instance before rotation/recreation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find the views in the layout
        mMovieGrid = findViewById(R.id.movie_grid);
        mProgressBar = findViewById(R.id.progress_loading_movies);
        mErrorTextView = findViewById(R.id.error_loading_movies);

        // initialize the adapter
        mMovieGridAdapter = new MovieGridAdapter(this, this);

        // arrange the movies in grid layout
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);

        // set the layoutmanager and adapter of the recyclerview
        mMovieGrid.setLayoutManager(layoutManager);
        mMovieGrid.setHasFixedSize(true);
        mMovieGrid.setAdapter(mMovieGridAdapter);

        // get movie sort order if set in saved instance state
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(INSTANCE_STATE_KEY_SORT_ORDER)) {
                mMovieSortOrder = savedInstanceState.getString(INSTANCE_STATE_KEY_SORT_ORDER);
            }
        }

        // get the movie-list from the tmdb api
        getMovies();
    }

    /**
     * Save the state
     *
     * @param outState Saved state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(INSTANCE_STATE_KEY_SORT_ORDER, mMovieSortOrder);
    }

    /**
     * Create/update the actions menu items and check the selected sort-order
     *
     * @param menu The Menu object
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        new MenuInflater(getApplicationContext()).inflate(R.menu.main, menu);
        MenuItem itemSortTopRated = null;
        MenuItem itemPopular = null;

        // get the action menu items
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() == R.id.action_sort_top_rated) {
                itemSortTopRated = menu.getItem(i);
            } else if (menu.getItem(i).getItemId() == R.id.action_sort_popular) {
                itemPopular = menu.getItem(i);
            }
        }

        // check the selected action menu item, based on the selected sort-order
        if (itemSortTopRated != null && itemPopular != null) {
            if (mMovieSortOrder.equals(MOVIE_SORT_TOP_RATED)) {
                itemSortTopRated.setCheckable(true);
                itemSortTopRated.setChecked(true);
                itemPopular.setCheckable(false);
                itemPopular.setChecked(false);
            } else if (mMovieSortOrder.equals(MOVIE_SORT_POPULAR)) {
                itemSortTopRated.setCheckable(false);
                itemSortTopRated.setChecked(false);
                itemPopular.setCheckable(true);
                itemPopular.setChecked(true);
            }
        }

        return true;
    }

    /**
     * Act on the selected action menu item
     *
     * @param item Selected menu item
     * @return Return true when item found
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // check the menu item id
        if (item.getItemId() == R.id.action_sort_popular) {
            mMovieSortOrder = MOVIE_SORT_POPULAR;
            getMovies();
            invalidateOptionsMenu();
            return true;
        } else if (item.getItemId() == R.id.action_sort_top_rated) {
            mMovieSortOrder = MOVIE_SORT_TOP_RATED;
            getMovies();
            invalidateOptionsMenu();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Show the RecyclerView movie grid and hide the error TextView
     */
    private void showMovies() {
        mMovieGrid.setVisibility(View.VISIBLE);
        mErrorTextView.setVisibility(View.INVISIBLE);
    }

    /**
     * Hide the RecyclerView movie grid and show the error TextView
     */
    private void showError(String error) {
        if (error != null) {
            mErrorTextView.setText(error);
        } else {
            mErrorTextView.setText(getString(R.string.error_loading_movies));
        }
        mMovieGrid.setVisibility(View.INVISIBLE);
        mErrorTextView.setVisibility(View.VISIBLE);
    }

    /**
     * Load the movies from the TMDb Api
     */
    private void getMovies() {
        if (Utility.isNetworkAvailable(this)) {

            // clear the adapter
            mMovieGridAdapter.clearMovies();

            // show the recyclerview movie grid and hide the error textview
            showMovies();

            // show the progressbar
            mProgressBar.setVisibility(View.VISIBLE);

            // build the retrofit object with a gson converter
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(getString(R.string.tmdb_api_base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            TMDbApi api = retrofit.create(TMDbApi.class);

            // enqueue the call async and handle the response using a callback
            Call<JsonObject> movies;
            if (mMovieSortOrder.equals(MOVIE_SORT_POPULAR)) {
                movies = api.getPopularMovies(getString(R.string.tmdb_api_key));
            } else {
                movies = api.getTopRatedMovies(getString(R.string.tmdb_api_key));
            }
            movies.enqueue(mGetMoviesCallback);

        } else {
            showError(getString(R.string.error_network_unavailable));
        }
    }

    /**
     * Handle the onClick on the movie grid from the moviegrid adapter and start the DetailActivity
     * with an intent containing an ApiMovie object
     *
     * @param movie ApiMovie object
     */
    @Override
    public void onClick(ApiMovie movie) {
        Intent detailActivityIntent = new Intent(this, DetailActivity.class);
        detailActivityIntent.putExtra(INTENT_EXTRA_MOVIE, movie);
        startActivity(detailActivityIntent);
    }

    /**
     * Callback that will either process the response from the TMDb Api, or throw an error
     */
    private final Callback<JsonObject> mGetMoviesCallback = new Callback<JsonObject>() {

        /**
         * Process the TMDb Api response and send a List of movie-objects to the MovieGridAdapter
         * @param call Original Retrofit Call object
         * @param response Response received from the Api
         */
        @Override
        public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
            mProgressBar.setVisibility(View.INVISIBLE);
            JsonObject body = response.body();
            List<ApiMovie> movies = new ArrayList<>();

            // get the results array from the json response and transform it into a List of ApiMovie objects
            if (body != null &&
                    body.isJsonObject() &&
                    body.has(getString(R.string.tmdb_api_call_movies_response_element_results))) {
                JsonArray results = body.getAsJsonArray(
                        getString(R.string.tmdb_api_call_movies_response_element_results));
                if (results != null && results.size() > 0) {
                    for (JsonElement movieElement : results) {
                        ApiMovie movie = new Gson().fromJson(movieElement, ApiMovie.class);
                        if (movie != null) {
                            movies.add(movie);
                        }
                    }
                }
            }

            // if at least one movie found, send the List to the adapter, or else show an error
            if (movies.size() > 0) {
                mMovieGridAdapter.setMovies(movies);
            } else {
                showError(null);
            }
        }

        /**
         * Handle the thrown error on failure of the Api call
         * @param call Original Retrofit Call object
         * @param t The error that was thrown
         */
        @Override
        public void onFailure(@NonNull Call<JsonObject> call, @NonNull Throwable t) {
            mProgressBar.setVisibility(View.INVISIBLE);
            Log.e(LOG_TAG, t.getMessage());
            showError(null);
        }
    };
}
