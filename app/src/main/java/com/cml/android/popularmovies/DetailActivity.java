package com.cml.android.popularmovies;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.cml.android.popularmovies.Api.ApiMovie;
import com.squareup.picasso.Picasso;

/**
 * DetailActivity containing the details of movie selected in the MainActivity movie grid
 */
public class DetailActivity extends AppCompatActivity {

    /**
     * ApiMovie object containing the movie details
     */
    private ApiMovie mMovie;

    /**
     * View items
     */
    private TextView mMovieTitle;
    private ImageView mMovieThumbnail;
    private TextView mMovieDate;
    private TextView mMovieVoteAverage;
    private TextView mMovieOverview;

    /**
     * Instantiate the view components and get the movie details from the intent
     *
     * @param savedInstanceState The state of the instance before rotation/recreation
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        // get the view items
        mMovieTitle = findViewById(R.id.movie_title);
        mMovieThumbnail = findViewById(R.id.movie_thumbnail);
        mMovieDate = findViewById(R.id.movie_date);
        mMovieVoteAverage = findViewById(R.id.movie_vote_average);
        mMovieOverview = findViewById(R.id.movie_overview);

        // get the calling intent and extract the ApiMovie Parcelable containing the movie details
        Intent callingIntent = getIntent();
        if (callingIntent.hasExtra(MainActivity.INTENT_EXTRA_MOVIE)) {
            mMovie = callingIntent.getParcelableExtra(MainActivity.INTENT_EXTRA_MOVIE);

            // title
            mMovieTitle.setText(mMovie.getTitle());

            // thumbnail
            String imageUrl = getString(R.string.tmdb_image_base_url)
                    + getString(R.string.tmdb_image_format_medium)
                    + mMovie.getPoster_path();
            Picasso.with(this)
                    .load(imageUrl)
                    .error(R.drawable.tmdb_icon_small)
                    .into(mMovieThumbnail);

            // date
            mMovieDate.setText(mMovie.getRelease_date());

            // vote average
            mMovieVoteAverage.setText(String.valueOf(mMovie.getVote_average()) + "/10");

            // overview
            mMovieOverview.setText(mMovie.getOverview());
        }
    }

    /**
     * Act on the Toolbar back-button MenuItem and make the transition equal to the Android
     * back-button animation
     *
     * @param item Clicked MenuItem
     * @return success/failure boolean
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // if menu item is toolbar back-button
        if (item.getItemId() == android.R.id.home) {

            // only works on api level higher then 21
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                finishAfterTransition();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
